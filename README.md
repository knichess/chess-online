# Chess Webapp

This is a chess platform with multiple rooms where you can play this great game with your friends.
Gallery : http://imgur.com/a/p2Mf3

## Task List

### General

- [x] ~~Create git repository~~
- [x] ~~in GameSerializer, you return "---" if the player is set to None, whereas "---" is also a valid player name, which will eventualy cause confusion. Ideally it would be great if you returned None, but if it has to be a string make it "$$$" at least.~~

### Frontend

- [x] ~~Setup frontend build system~~
- [ ] Fix 404.html
- [ ] Add favicon.ico
- [ ] Figure out PhantomJS

### Somewhere in the middle
- [x] ~~Interface between front-end and back-end: on the top of my head, a function to create a new game given a User...~~
- [x] ~~...and joinig an existing game as a player/spectator~~

### Backend

- [x] Setup backend management system
- [x] robots.txt

## Build & Development

TODO
- install python, ruby, node [nvm]
- install redis, sass
- pip install -r requirements.txt
- cd frontend

- [nvm install node]
- npm install -g bower grunt-cli
- npm install
- bower install

- grunt build

- cd ..
- ./manage.py migrate
- ./manage.py runserver
- ./manage.py runsd
- redis-server

## Deployment

- ./manage.py createsuperuser