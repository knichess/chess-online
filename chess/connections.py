from importlib import import_module
from django.conf import settings
from django.contrib.auth import get_user
from django.contrib.auth.models import AnonymousUser
from swampdragon.connections.sockjs_connection import DjangoSubscriberConnection


class _SessionWrapper(object):
    def __init__(self, session):
        self.session = session


class RouterConnection(DjangoSubscriberConnection):
    def __init__(self, session):
        super(RouterConnection, self).__init__(session)
        self.engine = import_module(settings.SESSION_ENGINE)

    def on_open(self, info):
        session_cookie = info.cookies.get(settings.SESSION_COOKIE_NAME, None)
        if session_cookie:
            session = self.engine.SessionStore(session_cookie.value)
            self.user = get_user(_SessionWrapper(session))
        else:
            self.user = AnonymousUser();
