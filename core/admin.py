from django.contrib import admin
from .models import Game, HistoryOfMoves, CurrentGameState


class GameAdmin(admin.ModelAdmin):
	list_display=["__str__","player_black","player_white"]

class HistoryOfMovesAdmin(admin.ModelAdmin):
	list_display=["__str__","color","from_field","to_field","timestamp"]

class CurrentGameStateAdmin(admin.ModelAdmin):
	list_display=["__str__","col_0","col_1","col_2","col_3","col_4","col_5","col_6","col_7","updated"]


admin.site.register(Game,GameAdmin)
admin.site.register(HistoryOfMoves,HistoryOfMovesAdmin)
admin.site.register(CurrentGameState,CurrentGameStateAdmin)
