from .models import CurrentGameState, HistoryOfMoves

class chess_logic():

	def oppesite_color(self,color):
		if color == 'white':
			return 'black'
		elif color == 'black': 
			return 'white'

	def current_state_array(self,game):#(game_id)

		"""
		Transforms Database records of CurrentGameState to nice array 8x8.
		"""

		queryset = CurrentGameState.objects.all().filter(game_id=game.id)

		board=[[getattr(row, 'col_' + str(i)) for i in range(8)] for row in queryset]

		for i in xrange(8):
			for j in xrange(8):
				if board[i][j]=='':
					board[i][j]=None

		return board

	def game_state(self, game):
		"""
		Preapare board to send
		"""
		board =  self.current_state_array(game)

		color = {'b': 'black', 'w': 'white'}
		typ = {'ro': 'tower','kn': 'knight','bi': 'bishop', 'qu': 'queen', 'ki': 'king', 'pa': 'pawn'}

		"""
		Colors and pieces name in human form

		color[board[0][0][2]]
		typ[board[0][i][:2]]
		"""

		trans = {'board':[]}

		for i in xrange(8):
			trans['board'].append([])
			for j in xrange(8):
				if board[i][j] is not None:
					trans['board'][i].append({'color': color[board[i][j][2]], 'type': typ[board[i][j][:2]]})
				else :
					trans['board'][i].append(None)
		return trans

	def movable_pieces(self, game, user):
		board = self.current_state_array(game)
		color = {'b': 'black', 'w': 'white'}
		typ = {'ro': 'tower','kn': 'knight','bi': 'bishop', 'qu': 'queen', 'ki': 'king', 'pa': 'pawn'}

		trans={'movables': []}

		if not HistoryOfMoves.objects.filter(game_id = game).exists():
			turn = 'white'
		else:
			last_color = HistoryOfMoves.objects.filter(game_id = game)[0].color
			turn = self.oppesite_color(last_color)

		trans = {'movables':[]}
		for i in xrange(8):
			trans['movables'].append([])
			for j in xrange(8):
				trans['movables'][i].append(False)

		if game.player_white==user and game.player_black==user:
			for i in xrange(8):
				for j in xrange(8):
					if board[i][j] is not None:
						if board[i][j][2] == turn[0]:
							trans['movables'][i][j] = True
		else:
			if game.player_white==user:
				status = "white"
			elif game.player_black==user:
				status = "black"

			if status == turn:
				for i in xrange(8):
					trans['movables'].append([])
					for j in xrange(8):
						if board[i][j] is not None:
							if color[board[i][j][2]] == status:
								trans['movables'][i][j] = True
							else :
								trans['movables'][i][j] = False
						else :
							trans['movables'][i][j] = False

		return trans

	def SendAllowedFields(self, game,user,piece):
		
		arr = self.allowed_fields(game,user,piece)
		
		trans={'targets': [[False for x in range(8)] for x in range(8)]}
		

		for i in arr:
			print i
			trans['targets'][i[0]][i[1]]=True

		return trans		

	def allowed_fields(self, game, user, piece):

		"""
		Returns an array cointaining sets of possible to move fields.
		It cointains also a fields that are possible to capture.
		Posx,posy is from 0-7 and represents a-h,1-8
		"""

		board = self.current_state_array(game)
		pos_x = piece['x']
		pos_y = piece['y']
		name = board[pos_y][pos_x][:2]

		if board[pos_y][pos_x][2] == 'w' and game.player_white == user:
			color = 'white'
		elif board[pos_y][pos_x][2] == 'b' and game.player_black == user:
			color = 'black'

		redirection = {
		'pa': self.pa_moves(pos_y, pos_x, color, board, game),
		'ro': self.ro_moves(pos_y, pos_x, color, board, game),
		'kn': self.kn_moves(pos_y, pos_x, color, board, game),
		'bi': self.bi_moves(pos_y, pos_x, color, board, game),
		'qu': self.qu_moves(pos_y, pos_x, color, board, game),
		'ki': self.ki_moves(pos_y, pos_x, color, board, game)
		}
		arr=redirection[name]

		return arr

	def pa_moves(self, pos_y, pos_x, color, board, game):
		fields=[]

		x_change = {'0':'a','1':'b','2':'c','3':'d','4':'e','5':'f','6':'g','7':'h'}
		y_change = {'0':'8','1':'7','2':'6','3':'5','4':'4','5':'3','6':'2','7':'1'}
		field = x_change[str(pos_x)] + y_change[str(pos_y)]
		
		if color == 'white':
			#First move
			if pos_y == 6 and board[pos_y-2][pos_x] == None and board[pos_y-1][pos_x] == None:
				fields.append([pos_y-2,pos_x])

			#Diagonally
			if pos_y>0:
				if pos_x > 0:
					if board[pos_y-1][pos_x-1] != None:
						if board[pos_y-1][pos_x-1][2] == 'b':
							fields.append([pos_y-1,pos_x-1])
				if pos_x < 7:
					if board[pos_y-1][pos_x+1] != None:
						if board[pos_y-1][pos_x+1][2] == 'b':
							fields.append([pos_y-1,pos_x+1])

				#Normal move
				if board[pos_y-1][pos_x] == None:
					fields.append([pos_y-1,pos_x])

			#EP
			if pos_y == 6:
				if pos_x < 7:
					if board[pos_y-2][pos_x+1] != None:
						if board[pos_y-2][pos_x+1][2] == 'b':
							fields.append([pos_y-2,pos_x+1])
				if pos_x >0:
					if board[pos_y-2][pos_x-1] != None:
						if board[pos_y-2][pos_x-1][2] == 'b':
							fields.append([pos_y-2,pos_x-1])

		elif color == 'black':

			#First move
			if pos_y == 1 and board[pos_y+2][pos_x] == None and board[pos_y+1][pos_x] == None:
				fields.append([pos_y+2,pos_x])
			#Diagonally
			if pos_y < 7:
				
				if pos_x > 0:
					if board[pos_y+1][pos_x-1] != None:
						if board[pos_y+1][pos_x-1][2] == 'w':
							fields.append([pos_y+1,pos_x-1])

				if pos_x < 7:
					if board[pos_y+1][pos_x+1] != None:
						if board[pos_y+1][pos_x+1][2] == 'w':
							fields.append([pos_y+1,pos_x+1])
				#Normal move
				if board[pos_y+1][pos_x] == None:
					
					fields.append([pos_y+1,pos_x])
			
			if pos_y == 1:
				if pos_x < 7:
					if board[pos_y+2][pos_x+1] != None:
						if board[pos_y+2][pos_x+1][2] == 'w':
							fields.append([pos_y+2,pos_x+1])
				if pos_x > 0:
					if board[pos_y+2][pos_x-1] != None:
						if board[pos_y+2][pos_x-1][2] == 'w':
							fields.append([pos_y+2,pos_x-1])
		return fields

	def ro_moves(self, pos_y, pos_x, color,board, game):
		

		fields = []

		#UP

		for i in range(pos_y-1,-1,-1):
			if board[i][pos_x] == None:
				fields.append([i,pos_x])
			else:
				if board[i][pos_x][2] != color[0]:
					fields.append([i,pos_x])	
				break

		#DOWN
		for i in range(pos_y+1,8):
			if board[i][pos_x] == None:
				fields.append([i,pos_x])
			else:
				if board[i][pos_x][2] != color[0]:
					fields.append([i,pos_x])	
				break
		#RIGHT
		for i in range(pos_x+1,8):
			if board[pos_y][i] == None:
				fields.append([pos_y,i])
			else:
				if board[pos_y][i][2] != color[0]:
					fields.append([pos_y,i])	
				break

		#LEFT
		for i in range(pos_x-1,-1,-1):
			if board[pos_y][i] == None:
				fields.append([pos_y,i])
			else:
				if board[pos_y][i][2] != color[0]:
					fields.append([pos_y,i])	
				break


		return fields

	def kn_moves(self, pos_y, pos_x, color,board, game):

		fields = []

		def checking_and_adding(y,x):
			if board[y][x] == None:#clear
				fields.append([y,x])
			elif board[y][x][2] != color[0]:#capture
				fields.append([y,x])

		#Up one
		if pos_y-1 >= 0 :
			#Up one and two right
			if pos_x+2 <= 7:
				checking_and_adding(pos_y-1,pos_x+2)
			#Up one and two left
			if pos_x-2 >= 0:
				checking_and_adding(pos_y-1,pos_x-2)
			#Up two
			if pos_y-2 >= 0:
				#Up two and one left
				if pos_x-1 >= 0:
					checking_and_adding(pos_y-2,pos_x-1)
				#Up two and one right
				if pos_x+1 <= 7:
					checking_and_adding(pos_y-2,pos_x+1)

		#Down one
		if pos_y+1 <= 7:
			#Down one and two right
			if pos_x+2 <= 7:
				checking_and_adding(pos_y+1,pos_x+2)
			#Down one and two left
			if pos_x-2 >= 0:
				checking_and_adding(pos_y+1,pos_x-2)
			#Down two
			if pos_y+2 <= 7:
				#Down two and one left
				if pos_x-1 >= 0:
					checking_and_adding(pos_y+2,pos_x-1)
				#Down two and one right
				if pos_x+1 <= 7:
					checking_and_adding(pos_y+2,pos_x+1)

		return fields

	def bi_moves(self,pos_y, pos_x, color, board, game):

		fields = []

		# Up left
		for y,x in zip(range(pos_y-1,-1,-1),range(pos_x-1,-1,-1)):
			
			if board[y][x] == None:#clear
				fields.append([y,x])
			else:
				if board[y][x][2] != color[0]: #capture
					fields.append([y,x])
				break
		# Up right
		for y,x in zip(range(pos_y-1,-1,-1),range(pos_x+1,8)):
			
			if board[y][x] == None:#clear
				fields.append([y,x])
			else:
				if board[y][x][2] != color[0]: #capture
					fields.append([y,x])
				break
		# Down left
		for y,x in zip(range(pos_y+1,8),range(pos_x-1,-1,-1)):
			
			if board[y][x] == None:#clear
				fields.append([y,x])
			else:
				if board[y][x][2] != color[0]: #capture
					fields.append([y,x])
				break	
		#Down right
		for y,x in zip(range(pos_y+1,8),range(pos_x+1,8)):
			
			if board[y][x] == None:#clear
				fields.append([y,x])
			else:
				if board[y][x][2] != color[0]: #capture
					fields.append([y,x])

		return fields

	def qu_moves(self,pos_y, pos_x, color, board, game):

		fields = []

		for i in self.bi_moves(pos_y, pos_x, color, board, game):
			fields.append(i)
		for j in self.ro_moves(pos_y, pos_x, color, board, game):
			fields.append(j)

		return fields

	def ki_moves(self,pos_y, pos_x, color, board, game, check = False):

		
		fields = []
		
		if check == False:

			danger_fields = self.EnemyCapturing(pos_y, pos_x, color, board, game)
			
			def checking_and_adding(y,x):
				if [y,x] not in danger_fields: 
					if board[y][x] == None:#clear
						fields.append([y,x])
					elif board[y][x][2] != color[0]:#capture
						fields.append([y,x])
		elif check == True:
			def checking_and_adding(y,x):
				if board[y][x] == None:#clear
					fields.append([y,x])
				elif board[y][x][2] != color[0]:#capture
					fields.append([y,x])

		if pos_y+1 < 8:
			#Down left
			if(pos_x-1 >= 0):
				checking_and_adding(pos_y+1,pos_x-1)
			#Down
			checking_and_adding(pos_y+1,pos_x)
			#Down right
			if(pos_x+1 <= 7):
				checking_and_adding(pos_y+1,pos_x+1)
		if pos_y-1 >= 0:
			#Up left
			if(pos_x-1 >= 0):
				checking_and_adding(pos_y-1,pos_x-1)
			#Up
			checking_and_adding(pos_y-1,pos_x)
			#Up right
			if pos_x+1 <= 7:
				checking_and_adding(pos_y-1,pos_x+1)
		#Left
		if pos_x+1 <= 7:
			checking_and_adding(pos_y, pos_x+1)
		#Right
		if pos_x-1 >= 0:
			checking_and_adding(pos_y, pos_x-1)

		#Castling
		if color == 'white':
			if not HistoryOfMoves.objects.filter(game_id = game, from_field = 'e1' ).exists():			
				if pos_y == 7 and pos_x == 4 :
					if board[7][1] == None and board[7][2] == None and board[7][3] == None:
						if board[7][0] == 'row':
							fields.append([7,0])
					if board[7][6] == None and board[7][5] == None:
						if board[7][7] == 'row':
							fields.append([7,7])

		elif color == 'black':
			if not HistoryOfMoves.objects.filter(game_id = game, from_field = 'e8' ).exists():
				if pos_y == 0 and pos_x == 4:
					if board[0][1] == None and board[0][2] == None and board[0][3] == None:
						if board[0][0] == 'rob':
							fields.append([0,0])
					if board[0][6] == None and board[0][5] == None:
						if board[0][7] == 'rob':
							fields.append([0,7])
		return fields

	def EnemyCapturing(self,pos_y, pos_x, color, board, game):

		danger_fields = []
		
		#Gotta change color cause im checking enemy
		if color == 'white':
			color = 'black'
		elif color == 'black':
			color = 'white'

		class_name = chess_logic()
		for i in xrange(8):
			for j in xrange(8):
				if board[i][j] != None:
					if board[i][j][2] == color[0]:
						if board[i][j][:2] == 'ki':
							temp_danger = self.ki_moves(i, j, color, board, game, True)
							for k in temp_danger:
								danger_fields.append(k)
						else:
							name = board[i][j][:2]+'_moves'
							#Here it use to brake. For unknown reasons last black paw was as None 
							#print getattr(class_name,name)(i, j, color, board)
							temp_danger=getattr(class_name,name)(i, j, color, board, game)
							for k in temp_danger:
								danger_fields.append(k)
		return danger_fields

	def check_moves(self , game, user , from_field , to_field ):
		
		board = self.current_state_array(game)
		name = board[from_field['y']][from_field['x']]

		# Good be also to check if this piece is from movable_pieces

		if [to_field['y'],to_field['x']] in self.allowed_fields(game,user,from_field):
			if name[:2] == 'pa' and (to_field['y'] == 0 or to_field['y'] == 7):
				return 'promote'
			else:
				return True
		else:
			return False


	def MakeAMove(self, game, user, from_field, to_field, promote = None):

		board = self.current_state_array(game)
		name = board[from_field['y']][from_field['x']]

		if board[from_field['y']][from_field['x']][2] == 'b':
			if game.player_black == user:
				color = 'black'
		elif game.player_white == user:
			color = 'white'

		column = 'col_'+str(from_field['x'])
		column_update = 'col_'+str(to_field['x'])
		
		promotion_list = {'queen': 'qu', 'bishop': 'bi', 'knight': 'kn', 'tower': 'ro'}

		#Promotion
		if name[:2] == 'pa' and promote != None and promote in promotion_list:

			new_name = promotion_list[promote]+color[0]

			queryset = CurrentGameState.objects.all().filter(game_id = game.id, row_id = from_field['y'])\
			.update(**{column: ""})

			queryset = CurrentGameState.objects.all().filter(game_id = game.id, row_id = to_field['y'])\
			.update(**{column_update: new_name})

		#CASTLING
		elif name[:2] == 'ki' and board[to_field['y']][to_field['x']] != None:
			if board[to_field['y']][to_field['x']][:2] == 'ro':
				to_name = board[to_field['y']][to_field['x']]

				queryset = CurrentGameState.objects.all().filter(game_id = game.id, row_id = from_field['y'])\
				.update(**{column: to_name})

				queryset = CurrentGameState.objects.all().filter(game_id = game.id, row_id = to_field['y'])\
				.update(**{column_update: name})

		#NORMALL
		else:
			queryset = CurrentGameState.objects.all().filter(game_id = game.id, row_id = from_field['y'])\
			.update(**{column: ""})

			queryset = CurrentGameState.objects.all().filter(game_id = game.id, row_id = to_field['y'])\
			.update(**{column_update: name})

		

		x_change = {'0':'a','1':'b','2':'c','3':'d','4':'e','5':'f','6':'g','7':'h'}
		y_change = {'0':'8','1':'7','2':'6','3':'5','4':'4','5':'3','6':'2','7':'1'}

		start = x_change[str(from_field['x'])]+y_change[str(from_field['y'])]
		end = x_change[str(to_field['x'])]+y_change[str(to_field['y'])]
		

		HistoryOfMoves(game_id = game, color = color, from_field = start, to_field = end).save()
		
