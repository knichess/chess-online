from django.db import models
from django import forms
from django.contrib.auth.models import User

from swampdragon.models import SelfPublishModel

from .serializers import GameSerializer

class Game(SelfPublishModel, models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=128)
    player_black = models.ForeignKey(User, related_name='game_as_black',blank = True, null = True)
    player_white = models.ForeignKey(User, related_name='game_as_white',blank = True, null = True)
    created_at = models.DateTimeField(auto_now_add=True,auto_now=False)

    serializer_class = GameSerializer

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        # TODO: not only called once fix

        super(Game, self).save(*args, **kwargs)

        if not CurrentGameState.objects.filter(game_id=self).exists():
	    	CurrentGameState(game_id=self,row_id=7,col_0="row",col_1="knw",col_2="biw",col_3="quw",col_4="kiw",col_5="biw",col_6="knw",col_7="row").save()
	    	CurrentGameState(game_id=self,row_id=6,col_0="paw",col_1="paw",col_2="paw",col_3="paw",col_4="paw",col_5="paw",col_6="paw",col_7="paw").save()
	    	CurrentGameState(game_id=self,row_id=5).save()
	    	CurrentGameState(game_id=self,row_id=4).save()
	    	CurrentGameState(game_id=self,row_id=3).save()
	    	CurrentGameState(game_id=self,row_id=2).save()
	    	CurrentGameState(game_id=self,row_id=1,col_0="pab",col_1="pab",col_2="pab",col_3="pab",col_4="pab",col_5="pab",col_6="pab",col_7="pab").save()
	    	CurrentGameState(game_id=self,row_id=0,col_0="rob",col_1="knb",col_2="bib",col_3="qub",col_4="kib",col_5="bib",col_6="knb",col_7="rob").save()

class HistoryOfMoves(models.Model):
	id = models.AutoField(primary_key=True)
	game_id = models.ForeignKey(Game)
	#player = models.ForeignKey(User)
	color = models.CharField(max_length = 5)
	from_field = models.CharField(max_length = 2)
	to_field = models.CharField(max_length = 2)
	timestamp = models.DateTimeField(auto_now_add=True,auto_now=False)
	#updated = models.DateTimeField(auto_now_add=False,auto_now=True)

	class Meta:
		ordering = ["-timestamp"]
		verbose_name_plural = "History of moves"

	def __str__(self):
		return str(self.game_id)

class EmptyStringToNoneField(models.CharField):
    def get_prep_value(self, value):
    	field=value
        if value == '':
            return None
        return value

class CurrentGameState(models.Model):
	"""
	Stores information about the current state of each game. Each column is a row and is created by col_+str(number)
	"""
	id = models.AutoField(primary_key=True)
	game_id = models.ForeignKey(Game)
	row_id = models.IntegerField()

	col_0 = models.CharField(max_length=3, blank=True, null=True)
	col_1 = models.CharField(max_length=3, blank=True, null=True)
	col_2 = models.CharField(max_length=3, blank=True, null=True)
	col_3 = models.CharField(max_length=3, blank=True, null=True)
	col_4 = models.CharField(max_length=3, blank=True, null=True)
	col_5 = models.CharField(max_length=3, blank=True, null=True)
	col_6 = models.CharField(max_length=3, blank=True, null=True)
	col_7 = models.CharField(max_length=3, blank=True, null=True)

	updated = models.DateTimeField(auto_now_add=False,auto_now=True)

	class Meta:
		ordering = ["-game_id","row_id"]

	def __str__(self):
		return '\"{}\" - Row {}'.format(self.game_id.title,self.row_id)
