from swampdragon.permissions import RoutePermission


class LoginRequired(RoutePermission):
    def __init__(self, verbs=None):
        self.verbs = verbs

    def test_permission(self, handler, verb, **kwargs):
        if self.verbs and verb not in self.verbs:
            return True
        else:
            return handler.connection.user.is_authenticated()

    def permission_failed(self, handler):
        handler.send_login_required()


def login_required(function):
    def check_user(self, **kwargs):
        if not self.connection.user.is_authenticated():
            self.send_login_required()
        else:
            return function(self, **kwargs)
    return check_user
