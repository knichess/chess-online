import numbers

from swampdragon import route_handler
from swampdragon.route_handler import ModelRouter, BaseRouter
from swampdragon.route_handler import CHANNEL_DATA_SUBSCRIBE

from .models import Game
from .serializers import GameSerializer
from .permissions import login_required, LoginRequired

from chess_logic import chess_logic

class InvalidChannelException(Exception):
    pass


class InvalidVerbArgumentException(Exception):
    def __init__(self, message, subject):
        super(InvalidVerbArgumentException, self).__init__(self, message)
        self.subject = subject


class PrefixedChannelMixin(object):
    def get_channel_id(self, channel):
        return channel[len(self.channel_prefix):]

    def get_subscription_channels(self, **kwargs):
        if 'channel' not in kwargs:
            raise InvalidChannelException('channel has to be specified')

        channel = kwargs['channel']
        if (channel
            and channel.startswith(self.channel_prefix)
            and self.is_valid_channel_id(self.get_channel_id(channel))):
            return [channel]

        raise InvalidChannelException('{} is not a valid channel'.format(channel))


class SymetricRouter(BaseRouter):
    valid_verbs = ['subscribe', 'unsubscribe']

    def subscribe(self, **kwargs):
        try:
            client_channel = kwargs['channel']
            server_channels = self.get_subscription_channels(**kwargs)

            self.send(
                data='subscribed',
                channel_setup=self.make_channel_data(client_channel, server_channels, CHANNEL_DATA_SUBSCRIBE),
                **kwargs)
            self.connection.pub_sub.subscribe(server_channels, self.connection)
        except InvalidChannelException as e:
            self.send_error({'channel': str(e)})

    def unsubscribe(self, **kwargs):
        try:
            client_channel = kwargs['channel']
            server_channels = self.get_subscription_channels(**kwargs)

            self.send(
                data='unsubscribed',
                channel_setup=self.make_channel_data(client_channel, server_channels, CHANNEL_DATA_UNSUBSCRIBE),
                **kwargs
            )
            self.connection.pub_sub.unsubscribe(server_channels, self.connection)
        except InvalidChannelException as e:
            self.send_error({'channel': str(e)})

    def send_ok(self):
        self.send({'status': 'ok'})


class LobbyRouter(ModelRouter):
    route_name = 'lobby-route'
    serializer_class = GameSerializer
    model = Game

    def get_query_set(self, **kwargs):
        if 'query' in kwargs:
            if 'category' not in kwargs:
                return []

            selected = kwargs['category']
            if (not isinstance(selected, numbers.Integral)
                or selected < 0 or selected > 3):
                return []

            selectors = ['pk', 'title', 'player']
            if selected != 0:
                selectors = [selectors[selected-1]]

            if 'player' in selectors:
                selectors.remove('player')
                selectors.append('player_black__username')
                selectors.append('player_white__username')

            from django.db.models import Q
            return self.model.objects.filter(reduce(lambda tail, head: tail | head, [
                Q(**{selector + "__contains": kwargs['query']}) for selector in selectors
            ]))
        else:
            return self.model.objects.all()

    def get_object(self, **kwargs):
        return self.model.objects.get(id=kwargs['pk'])


class ChatRouter(PrefixedChannelMixin, SymetricRouter):
    channel_prefix = 'chat'
    route_name = 'chat-route'
    valid_verbs = SymetricRouter.valid_verbs + ['send_message']

    permission_classes = [LoginRequired()]

    def is_valid_channel_id(self, id):
        return id.isdigit()

    def send_message(self, **kwargs):
        try:
            if 'message' not in kwargs or len(kwargs['message']) is 0:
                raise InvalidVerbArgumentException('missing chat message', 'message')

            self.send_ok()
            self.publish(self.get_subscription_channels(**kwargs), {
                'message': kwargs['message'],
                'user': self.connection.user.username
            })
        except InvalidVerbArgumentException as e:
            self.send_error({e.subject: str(e)})


class GameRouter(PrefixedChannelMixin, SymetricRouter):
    channel_prefix = 'game'
    route_name = 'game-route'
    valid_verbs = SymetricRouter.valid_verbs + [
        'get_game_state',
        'get_movable_pieces',
        'get_possible_moves',
        'make_a_move'
    ]

    permission_classes = []

    def _get_game(self, **kwargs):
        if 'channel' not in kwargs:
            raise InvalidChannelException('channel has to be specified')

        channel = kwargs['channel']
        game_id = self.get_channel_id(channel)

        try:
            game = Game.objects.get(pk=game_id)
        except Game.DoesNotExist, ValueError:
            raise InvalidChannelException('{} is not a valid channel'.format(channel))
        return game

    def is_valid_channel_id(self, id):
        try:
            Game.objects.get(pk=id)
        except Game.DoesNotExist, ValueError:
            return False
        return True

    def get_game_state(self, **kwargs):
        # TODO acctual game state
        # TODO check possibility of xss
        # TODO add move number - sync
        try:
            game_id = self._get_game(**kwargs)
            self.send(chess_logic().game_state(game_id))
        except InvalidChannelException as e:
            self.send_error({'channel': str(e)})

    def get_movable_pieces(self, **kwargs):
        """
        Send dict with movables array 8x8 with true for every player piece.
        """
        try:
            game_id = self._get_game(**kwargs)
            user = self.connection.user
            if user.is_anonymous():
                self.send({
                    'movables': [[False for j in range(8)] for i in range(8)]
                })
            else:
                self.send(chess_logic().movable_pieces(game_id, user))
        except InvalidChannelException as e:
            self.send_error({'channel': str(e)})

    def _get_coords_bundle(self, name, **kwargs):
        if name not in kwargs:
            raise InvalidVerbArgumentException('has to be specified', name)

        bundle = kwargs[name]
        if 'x' not in bundle or 'y' not in bundle:
            raise InvalidVerbArgumentException('coordinates missing', name)
        if (not isinstance(bundle['x'], numbers.Integral)
            or not isinstance(bundle['y'], numbers.Integral)
            or bundle['x'] < 0
            or bundle['y'] < 0):
            raise InvalidVerbArgumentException('coordinates have to be positive integers', name)

        return bundle

    @login_required
    def get_possible_moves(self, **kwargs):
        """
        Send all moves that can be done by this piece (included capturing).
        """
        try:
            game_id = self._get_game(**kwargs)
            user = self.connection.user
            piece = self._get_coords_bundle('piece', **kwargs)

            self.send(chess_logic().SendAllowedFields(game_id, user, piece))
        except InvalidChannelException as e:
            self.send_error({'channel': str(e)})
        except InvalidVerbArgumentException as e:
            self.send_error({e.subject: str(e)})

    @login_required
    def make_a_move(self, **kwargs):
        """
        Actual move (from, to) and has to update CurrentState and add this to HistoryOfMoves
        """

        try:
            errors = {}
            game_id = self._get_game(**kwargs)
            user = self.connection.user
            move_from = self._get_coords_bundle('from', **kwargs)
            move_to = self._get_coords_bundle('to', **kwargs)

            # TODO fix the name and args ex. ChessLogic(game_id, user)
            game = chess_logic()

            if game.check_moves(game_id, user, move_from, move_to) == 'promote':
                if 'upgrade' not in kwargs or not kwargs['upgrade']:
                    self.send({
                        'action': 'upgrade'
                    })
                else:
                    upgrade = kwargs['upgrade']
                    if upgrade not in ['queen', 'bishop', 'knight', 'tower']:
                        raise InvalidVerbArgumentException('invalid choice', 'upgrade')

                    game.MakeAMove(game_id, user, move_from, move_to, upgrade)
                    self.send_ok()
                    self.publish(self.get_subscription_channels(**kwargs), game.game_state(game_id))

            elif game.check_moves(game_id, user, move_from, move_to) == True:
                game.MakeAMove(game_id, user, move_from, move_to)
                self.send_ok()
                self.publish(self.get_subscription_channels(**kwargs), game.game_state(game_id))
            else:
                self.send_error({'move': 'invalid'})

        except InvalidChannelException as e:
            self.send_error({'channel': str(e)})
        except InvalidVerbArgumentException as e:
            self.send_error({e.subject: str(e)})


route_handler.register(LobbyRouter)
route_handler.register(ChatRouter)
route_handler.register(GameRouter)
