from swampdragon.serializers.model_serializer import ModelSerializer
from django.db.models.aggregates import Max

import datetime
import math


class GameSerializer(ModelSerializer):
    class Meta:
        model = 'core.Game'
        publish_fields = ('id', 'title')

    def serialize_player_black(self, game):
        if game.player_black == None:
            return None
        return game.player_black.username

    def serialize_player_white(self, game):
        if game.player_white == None:
            return None
        return game.player_white.username

    def serialize_timestamp(self, game):
        last_move = game.historyofmoves_set.values('timestamp').annotate(last_move=Max('timestamp'))
        if len(last_move) > 0:
            return last_move[0]['last_move'].isoformat()
        else:
            return game.created_at.isoformat()

    def serialize_transcript(self, game):
        return list(game.historyofmoves_set.all().values('from_field', 'to_field', 'color'))
