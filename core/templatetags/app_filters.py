from django.template import Context
from django.template.loader import get_template
from django import template

register = template.Library()

@register.filter(name='add_css_class')
def add_css_class(field, css):
   return field.as_widget(attrs = { 'class' : css })

@register.filter(name='bootstrap_with_placeholder')
def bootstrap_with_placeholder(field, placeholder):
  return field.as_widget(attrs = { 'class' : 'form-control', 'placeholder' : placeholder })

@register.filter(name='bootstrap_form')
@register.inclusion_tag('bootstrap_form.html')
def bootstrap_form(form, csrf_token):
  template = get_template('global/bootstrap_form.html');
  context = Context({'form' : form, 'csrf_token' : csrf_token});
  return template.render(context)