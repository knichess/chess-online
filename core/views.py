from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.exceptions import PermissionDenied
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from .models import Game

from middle_engine import current_state_array


@login_required
def create_game(request):
    try:
        new_game = None

        if request.POST['game_title'].strip() == "":
            messages.error(request, 'Title cannot be empty or consist of whitespaces only')
            return HttpResponseRedirect('/')

        if request.POST["color_choice"] == 'white':
            new_game = Game(title=request.POST["game_title"], player_white=request.user)
        elif request.POST["color_choice"] == 'black':
            new_game = Game(title=request.POST["game_title"], player_black=request.user)

        if new_game:
            new_game.save()
            return HttpResponseRedirect('/game/' + str(new_game.pk))
        else:
            messages.error(request, 'There was an error while creating your game.')
            return HttpResponseRedirect('/')

    except Exception as e:
        messages.error(request, 'There was an error while creating your game.')
        return HttpResponseRedirect('/')


def join_game(request):
    game_id = request.POST['game_id']
    join_as = request.POST['join_as']

    if join_as == 'spectator':
        return HttpResponseRedirect('/game/' + game_id);

    if not request.user.is_authenticated():
        messages.error(request, 'You don\'t have permission to access this game.')
        return HttpResponseRedirect('/')

    game = Game.objects.get(id=game_id)
    if getattr(game, join_as) != None:
        messages.error(request, 'There already is a player you wanted to join as.')
        return HttpResponseRedirect('/')

    if join_as == 'player_black':
        game.player_black = request.user
        game.save()
        return HttpResponseRedirect('/game/' + game_id)
    elif join_as == 'player_white':
        game.player_white = request.user
        game.save()
        return HttpResponseRedirect('/game/' + game_id)

    messages.error(request, 'If this error occured something went seriously wrong. Turn off your computer and never turn it on again. Ever.')
    return HttpResponseRedirect('/')


@login_required
def exit_game(request):
    game_id = request.POST['game_id']
    exit_as = request.POST['exit_as']

    game = Game.objects.get(id=game_id)
    if getattr(game, exit_as) == None:
        messages.error(request, 'No one plays as that color, you can\'t finish playing it.')
        return HttpResponseRedirect('/')

    if exit_as == 'player_black' and game.player_black == request.user:
        game.player_black = None
    elif exit_as == 'player_white' and game.player_white == request.user:
        game.player_white = None
    else:
        messages.error(request, 'You can\'t exit game as someone else. Turn off your computer and never turn it on again.')
    game.save()
    return HttpResponseRedirect('/')


def lobby(request):
    return render(request, 'core/lobby.html')

def game(request, game_id):
    return render(request, 'core/game.html', {
        'game_id': game_id,
        'title': Game.objects.get(id=game_id).title
    })
