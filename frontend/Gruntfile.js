'use strict';

module.exports = function (grunt) {

  var serveStatic = require('serve-static');
  var proxy = require('grunt-connect-proxy/lib/utils').proxyRequest;

  require('time-grunt')(grunt);

  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin',
    ngtemplates: 'grunt-angular-templates',
    configureProxies: 'grunt-connect-proxy'
  });

  var projectConfig = {
    sources: 'sources',
    tests: 'tests',

    buildTemplates: 'templates',
    buildStatic: 'static',

    tmp: '.tmp',
    sassCache: '.sass-cache'
  };

  grunt.initConfig({

    project: projectConfig,

    clean: {
      build: {
        files: [{
          dot: true,
          src: [
            '<%= project.buildTemplates %>/**/*',
            '<%= project.buildStatic %>/**/*'
          ]
        }]
      },
      tmp: {
        files: [{
          dot: true,
          src: [
            '<%= project.tmp %>',
            '<%= project.sassCache %>'
          ]
        }]
      }
    },

    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      sources: {
        src: [
          'Gruntfile.js',
          '<%= project.sources %>/**/*.js'
        ]
      },
      tests: {
        options: {
          jshintrc: 'tests/.jshintrc'
        },
        src: ['<%= project.tests %>/**/*.js']
      }
    },

    jscs: {
      options: {
        config: '.jscsrc',
        verbose: true
      },
      sources: {
        src: [
          'Gruntfile.js',
          '<%= project.sources %>/**/*.js'
        ]
      },
      tests: {
        src: ['<%= project.tests %>/**/*.js']
      }
    },

    wiredep: {
      html: {
        src: ['<%= project.sources %>/**/*.html'],
        ignorePath:  /\.\.(\/\.\.)*/
      },
      sass: {
        src: ['<%= project.sources %>/**/*.{scss,sass}'],
        //ignorePath: /(\.\.\/)+\/bower_components\//
      },
      options: {
        exclude: ['bower_components/bootstrap-sass/assets/stylesheets/_bootstrap.scss'],
      }
    },

    sass: {
      sources: {
        files: [{
          expand: true,
          cwd: '<%= project.sources %>',
          src: '**/*.{scss,sass}',
          dest: '<%= project.tmp %>',
          ext: '.css'
        }]
      }
    },

    postcss: {
      options: {
        processors: [
          require('autoprefixer')({})
        ]
      },
      tmp: {
        options: {
          map: true
        },
        files: [{
          expand: true,
          cwd: '<%= project.tmp %>',
          src: '**/*.css',
          dest: '<%= project.tmp %>',
        }]
      }
    },

    useminPrepare: {
      options: {
        staging: '<%= project.tmp %>',
        root: '<%= project.sources %>',
        dest: '.'
      },
      sources: {
        src: 'sources/**/*.html'
      },
    },

    usemin: {
      html: [
        '<%= project.buildStatic%>/**/*.html',
        '<%= project.buildTemplates%>/**/*.html'
      ],
      css: [
        '<%= project.buildStatic%>/**/*.css',
        '<%= project.buildTemplates%>/**/*.css'
      ],
      js: [
        '<%= project.buildStatic%>/**/*.js',
        '<%= project.buildTemplates%>/**/*.js'
      ],
      options: {
        assetsDirs: [
          '<%= project.buildStatic %>',
          '<%= project.buildTemplates %>'
        ]
      }
    },

    copy: {
      robotsTxt: {
        files: [{
          expand: true,
          cwd: '<%= project.sources %>',
          dest: '.',
          src: [
            'templates/**/robots.txt'
          ]
        }]
      },
      angularTemplates: {
        files: [{
          expand: true,
          cwd: '<%= project.sources %>',
          dest: '.',
          src: [
            'static/**/*.ng.html'
          ]
        }]
      },
      staticHtml: {
        files: [{
          expand: true,
          cwd: '<%= project.sources %>',
          dest: '.',
          src: [
            'static/**/*.html',
            '!**/*.ng.html'
          ]
        }]
      },
      templatesHtml: {
        files: [{
          expand: true,
          cwd: '<%= project.sources %>',
          dest: '.',
          src: [
            'templates/**/*.html',
            '!**/*.ng.html'
          ]
        }]
      },
      assets: {
        files: [{
          expand: true,
          cwd: '<%= project.sources %>',
          dest: '.',
          src: [
            '**/*.{webp,png,jpg,jpeg,gif,svg,ico}',
            '**/*.{otf, ttf, woff*, eot}'
          ]
        }]
      },
      vendor: {
        files: [{
          expand: true,
          cwd: 'bower_components/bootstrap-sass/assets',
          src: 'fonts/**/*',
          dest: '<%= project.buildStatic %>/global'
        }]
      }
    },

    htmlmin: {
      build: {
        options: {
          collapseWhitespace: true,
          conservativeCollapse: true,
          collapseBooleanAttributes: true,
          removeCommentsFromCDATA: true
        },
        files: [{
          expand: true,
          cwd: '<%= project.buildStatic %>',
          src: '**/*.html',
          dest: '<%= project.buildStatic %>'
        }, {
          expand: true,
          cwd: '<%= project.buildTemplates %>',
          src: '**/*.html',
          dest: '<%= project.buildTemplates %>'
        }]
      }
    },

    ngAnnotate: {
      tmp: {
        files: [{
          expand: true,
          cwd: '.tmp/concat',
          src: '**/*.js',
          dest: '.tmp/concat'
        }]
      }
    },

    // TODO ngtemplates
    ngtemplates: {
      sources: {
        options: {
          module: 'chessOnline',
          htmlmin: '<%= htmlmin.build.options %>',
          usemin: 'static/**/*.js'
        },
        cwd: '<%= project.sources %>',
        src: ['**/*.ng.html'],
        dest: '.tmp/static/global/scripts/templates.js'
      }
    },

    connect: {
      options: {
        port: 9000,
        hostname: 'localhost',
        livereload: 35729
      },
      livereload: {
        options: {
          open: true,
          middleware: function (connect) {
            return [
              connect().use(
                '/bower_components',
                serveStatic('./bower_components')
              ),
              connect().use(
                '/static',
                serveStatic('./sources/static')
              ),
              connect().use(
                '/static',
                serveStatic('./.tmp/static')
              ),
              connect().use(
                '/static/global',
                serveStatic('bower_components/bootstrap-sass/assets')
              ),
              proxy
            ];
          }
        },
        proxies: [
            {
                context: '/',
                host: 'localhost',
                port: 8000,
                ws: true
            }
        ]
      }
    },

    watch: {
      gruntfile: {
        files: ['Gruntfile.js']
      },
      bower: {
        files: ['bower.json'],
        tasks: ['wiredep']
      },
      sass: {
        files: ['<%= project.sources %>/**/*.{scss,sass}'],
        tasks: ['sass', 'postcss']
      },
      js: {
        files: ['<%= project.sources %>/**/*.js'],
        tasks: ['newer:jshint', 'newer:jscs'],
        options: {
          livereload: '<%= connect.options.livereload %>'
        }
      },
      templates: {
        files: ['<%= project.sources %>/templates/**/*.html'],
        tasks: ['copy:templatesHtml'],
        options: {
          livereload: '<%= connect.options.livereload %>'
        }
      },
      livereload: {
        files: [
          '<%= project.sources %>/**/*.html',
          '.tmp/**/*.css',
          '<%= project.sources %>/**/*.{png,jpg,jpeg,gif,webp,svg}'
        ],
        options: {
          livereload: '<%= connect.options.livereload %>'
        }
      }
    },

  });

  grunt.registerTask('serve', 'Compile then start a connect web server with proxy to django', function (target) {

    //TODO serve release

    grunt.task.run([
      'clean:build',
      'wiredep',
      'sass',
      'postcss',
      'copy:templatesHtml',
      'copy:robotsTxt',
      'configureProxies:livereload',
      'connect:livereload',
      'watch'
    ]);
  });

  grunt.registerTask('build', [
    'clean',
    'wiredep',
    'useminPrepare',
    'sass',
    'postcss',
    // 'ngtemplates',
    'concat',
    'ngAnnotate',
    'copy',
    'cssmin',
    'uglify',
    'usemin',
    'htmlmin'
  ]);

  grunt.registerTask('default', [
    'newer:jshint',
    'newer:jscs',
    'build'
  ]);

};
