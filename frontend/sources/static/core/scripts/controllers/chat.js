'use strict';

/**
 * @ngdoc function
 * @name chessOnline.controller:ChatController
 * @description
 * # ChatController
 * Controller of the chat
 */
angular.module('chessOnline')
  .controller('ChatController', ['$scope', '$dragon', 'game_id', function($scope, $dragon, game_id) {
    $scope.channel = 'chat'+game_id;
    $scope.messages = [];
    $scope.connection_ready = false;

    $dragon.onReady(function() {

      $dragon.subscribe('chat-route', $scope.channel).catch(function(response) {
        log_dragon_error(response, 'unable to subscribe to chat');
      });

      $dragon.onChannelMessage(function(channels, message) {
        if($.inArray($scope.channel, channels) > -1) {
          $scope.$apply(function() {
            $scope.messages.push(message);
          });
        }
      });

      $scope.send_message = function(message) {
        $dragon.callRouter('send_message', 'chat-route', {'message': message}, $scope.channel).catch(function(response) {
          log_dragon_error(response, 'unable to send chat message');
        });
      };

      $scope.connection_ready = true;

    });
  }]);
