'use strict';

/**
 * @ngdoc function
 * @name chessOnline.controller:GameController
 * @description
 * # GameController
 * Controller of the game
 */
angular.module('chessOnline')
  .filter('reverse', function() {
    return function(items) {
      return items.slice().reverse();
    };
  })
  .controller('GameController', ['$scope', '$dragon', 'game_id', function($scope, $dragon, game_id) {
    $scope.rotate = false;
    $scope.channel = 'game'+game_id;
    $scope.connection_ready = false;
    var self = this;

    $scope.init = function(rotate) {
      $scope.rotate = rotate;
    };

    self.createArray = function(value, width, height) {
      width = width || 8;
      height = height || 8;
      return Array.apply(null, new Array(height)).map(function () {
        return Array.apply(null, new Array(width)).map(function () {
          return value;
        });
      });
    };

    self.resetMovables = function() {
      $scope.movables = self.createArray(false);
    };

    self.resetTargets = function() {
      $scope.targets = self.createArray(false);
    };

    $scope.selected = null;
    $scope.select = function(x, y) {
      self.updateTargets(x, y);
      $scope.selected = {
        'x': x,
        'y': y
      };
    };

    $scope.reset = function(x, y) {
      self.resetTargets();
    };

    $scope.move = function(x, y) {
      self.resetMovables();
      self.resetTargets();

      self.movePiece($scope.selected, {'x': x, 'y': y});
      $scope.board[y][x] = $scope.board[$scope.selected.y][$scope.selected.x];

      $scope.selected = null;
    };

    $scope.toggleRotation = function() {
      $scope.rotate = !$scope.rotate;
    };

    $scope.board = self.createArray(null);
    self.resetMovables();
    self.resetTargets();

    $dragon.onReady(function() {

      self.updateGameState = function() {
        $dragon.callRouter('get_game_state', 'game-route', {}, $scope.channel).then(function(response) {
          $scope.board = response.data.board;
        }).catch(function(response) {
          log_dragon_error(response, 'unable to download current state of the game');
        });
      };

      self.updateMovablePieces = function() {
        $dragon.callRouter('get_movable_pieces', 'game-route', {}, $scope.channel).then(function(response) {
          $scope.movables = response.data.movables;
        }).catch(function(response) {
          log_dragon_error(response, 'unable to download current state of the game');
        });
      };

      self.updateTargets = function(x, y) {
        $dragon.callRouter('get_possible_moves', 'game-route', {'piece': {'x': x, 'y': y}}, $scope.channel).then(function(response) {
          $scope.targets = response.data.targets;
        }).catch(function(response) {
          log_dragon_error(response, 'unable to download targets for piece at (' + x + ',' + y + ')');
        });
      };

      self.movePiece = function(from, to) {
        $dragon.callRouter('make_a_move', 'game-route', {'from': from, 'to': to}, $scope.channel).then(function(response) {
          if(response.data.action === 'upgrade') {
            $('#promotionModal').modal('show');
            $scope.upgrade = function(choice) {
              $dragon.callRouter('make_a_move', 'game-route', {'from': from, 'to': to, 'upgrade': choice}, $scope.channel).catch(function(response) {
                log_dragon_error(response, 'unable to make a move from ' + from + ' to ' + to);
              });
            };
          }
        }).catch(function(response) {
          log_dragon_error(response, 'unable to make a move from ' + from + ' to ' + to);
        });
      };

      $dragon.subscribe('game-route', $scope.channel).catch(function(response) {
        log_dragon_error(response, 'unable to subscribe to the game');
      });

      $dragon.subscribe('lobby-route', $scope.channel, {}).then(function(response) {
        self.dataMapper = new DataMapper(response.data);
      }).catch(function(response) {
        log_dragon_error(response, 'unable to subscribe to lobby');
      });

      $dragon.getSingle('lobby-route', {'pk': game_id}).then(function(response) {
        $scope.game_info = response.data;
      }).catch(function(response) {
        log_dragon_error(response, 'unable to load list of games');
      });

      $dragon.onChannelMessage(function(channels, message) {
        if($.inArray($scope.channel, channels) > -1) {
          $scope.$apply(function() {
            if(message.board) {
              $scope.board = message.board;
              self.updateMovablePieces();
            } else {
              self.dataMapper.mapData($scope.game_info, message);
            }
          });
        }
      });

      self.updateGameState();
      self.updateMovablePieces();
      $scope.connection_ready = true;

    });

  }]);
