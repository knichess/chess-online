'use strict';

/**
 * @ngdoc function
 * @name chessOnline.controller:GamesListController
 * @description
 * # GamesListController
 * Controller of the list of games
 */
angular.module('chessOnline')
  .controller('GamesListController', ['$scope', '$dragon', function($scope, $dragon) {
    $scope.channel = 'lobby';
    $scope.connection_ready = false;
    $scope.gamesList = [];
    var self = this;

    $scope.searchCategories = ['All', '#', 'Title', 'Player'];
    $scope.selectedSearchCategory = 0;

    $scope.search = function() {
      self.updateGamesList({
        'category': $scope.selectedSearchCategory,
        'query': $scope.query
      });
    };

    $dragon.onReady(function() {

      $dragon.subscribe('lobby-route', $scope.channel, {}).then(function(response) {
        self.dataMapper = new DataMapper(response.data);
      }).catch(function(response) {
        log_dragon_error(response, 'unable to subscribe to lobby');
      });

      $dragon.onChannelMessage(function(channels, message) {
        if ($.inArray($scope.channel, channels) > -1) {
          $scope.$apply(function() {
            self.dataMapper.mapData($scope.gamesList, message);
          });
        }
      });

      self.updateGamesList = function(args) {
        $dragon.getList('lobby-route', args || {}).then(function(response) {
            $scope.gamesList = response.data;
        }).catch(function(response) {
          log_dragon_error(response, 'unable to load list of games');
        });
      };

      self.updateGamesList();
      $scope.connection_ready = true;

    });

  }]);
