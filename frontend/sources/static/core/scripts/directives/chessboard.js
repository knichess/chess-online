'use strict';

/**
 * @ngdoc directive
 * @name chessOnline.directive:chessBoard
 * @description
 * # chessBoard
 */
angular.module('chessOnline')
  .directive('chessBoard', function() {
    return {
      templateUrl: '/static/core/ng-templates/chessboard.ng.html',
      restrict: 'A',
      replace: true,
      scope: {
        rotate: '=',
        board: '=',
        movables: '=',
        targets: '=',
        select: '&',
        move: '&',
        reset: '&'
      },
      controller: function($scope) {
        $scope.Math = window.Math;
        $scope.rot = function(coord) {
          return Math.abs($scope.rotate*7-coord);
        };

        $scope._move = function(event, ui, x, y) {
          $scope.move()(x, y);
        };

        $scope._select = function(event, ui, x, y) {
          $scope.select()(x, y);
        };

        $scope._reset = function(event, ui, x, y) {
          $scope.reset()(x, y);
        };
      }
    };
  });
