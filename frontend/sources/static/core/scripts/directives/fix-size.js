'use strict';

angular
  .module('chessOnline')
    .directive('fixSize', function($timeout, $dragon) {
      return {
          priority: Number.MIN_SAFE_INTEGER,
          restrict: "A",
          link: function($scope, $element, $attributes) {
            var $board = $element.find('.board');
            var $wrapper = $element.find('.wrapper');
            var $sidebar = $element.find('.sidebar');
            var $header = $element.find('.header');
            var $messages = $element.find('.messages');

            var $windowTop = $(window.top);

            var MAGIC1 = 100 + $('#game-title').height();
            var MESSAGES_PADDING = 12;

            var MAX_BOARD_WIDTH = 800;
            var MIN_CHAT_WIDTH = 200;

            var onWindowResize = function () {
              var height = $windowTop.height() - MAGIC1;
              var width = $element.width();
              var min = Math.min(height, width);
              min = min > MAX_BOARD_WIDTH ? MAX_BOARD_WIDTH : min;
              $board.width(min);
              $board.height(min);

              if (min + MIN_CHAT_WIDTH > width) {
                $sidebar
                  .width(min)
                  .height('auto')
                  .css('float', 'left')
                  .css('padding-left', '0px')
                  .css('padding-right', '0px')
                  .css('padding-top', '10px');
                $messages.height('auto');
                $wrapper.css('margin-left', 'auto');
                $wrapper.css('margin-right', 'auto');
                $wrapper.width(min);
              }
              else {
                var CHAT_PADDING_LEFT = 10;
                $sidebar
                  .width(width - min - CHAT_PADDING_LEFT)
                  .height(min)
                  .css('float', 'right')
                  .css('padding-left', CHAT_PADDING_LEFT + 'px')
                  .css('padding-right', '0px')
                  .css('padding-top', '0px');
                $messages.height(min - MESSAGES_PADDING - $header.outerHeight());
                $wrapper.css('margin', '0');
                $wrapper.width('auto');
              }
            };

            $timeout(function(){
              onWindowResize();
            });
            $(window).resize(onWindowResize);
          }
        };
    });
