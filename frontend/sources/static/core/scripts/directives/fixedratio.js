'use strict';

/**
 * @ngdoc directive
 * @name chessOnline.directive:fixedRatio
 * @description
 * # fixedRatio
 */
angular.module('chessOnline')
  .directive('fixedRatio', function() {
    return {
      template: '<div class="fixed-ratio"><div ng-transclude></div></div>',
      restrict: 'A',
      transclude: 'true',
      replace: 'true'
    };
  });
