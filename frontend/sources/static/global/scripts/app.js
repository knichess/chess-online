'use strict';

/**
 * @ngdoc overview
 * @name chessOnline
 * @description
 * # chessOnline
 *
 * Main module of the application.
 */
angular
  .module('chessOnline', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ngDragDrop',
    'SwampDragonServices'
  ])
  .config(function($interpolateProvider, $httpProvider) {
    // TODO
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
  });
