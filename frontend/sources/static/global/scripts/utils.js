'use strict';

var log_dragon_error = function(response, message) {
    var msg = 'error: ';
    msg += message + '\n[\n';
    if(response && response.errors) {
        for(var key in response.errors) {
          msg += '\t' + key + ': ' + response.errors[key] + '\n';
        }
    }
    msg += ']\n';
    console.log(msg);
};
