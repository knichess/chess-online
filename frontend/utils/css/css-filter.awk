#!/usr/bin/gawk -f

BEGIN {
	RS="}";
	FS="{";
	IGNORECASE=1;

	style_stack[0]="";
	style_stack[1]="";
	found[0]=0;
	found[1]=0;

	style_depth=1;
	if (ARGC<=1)
		settings_regex=".*";
	else {
		settings_regex=ARGV[1];
		ARGV[1]="";
	}
}

function find_last (string, pattern,		ret, pattern_index) {
	ret=0;
	while (1) {
		if (ret+1 > length(string))
			break;

		pattern_index=index(substr(string, ret+1), pattern);

		if (pattern_index==0)
			break;
		else ret+=pattern_index;
	}
	return ret;
}

function add_to_style (string) {
	if (style_stack[style_depth])
		style_stack[style_depth]=style_stack[style_depth] string;
	else style_stack[style_depth]=string;
}

function mark_setting (setting) {
	return setting ~ settings_regex
}

function repeat(string, n_times,	ret) {
	ret="";
	for (; n_times; n_times--)
		ret=ret string;
	return ret;
}

function create_indentation(string, offset,	depth) {
	gsub(/\s+/, " ", string);
	gsub(/\n+/, "\n", string);

	gsub(/^\s*|^\n*|\n*$|\s*$/, "", string);

	depth=style_depth+offset-1;
	if (depth < 0) depth = 0;
	return repeat("  ", depth) string;
}

function add_settings_to_style (settings_string, offset,	unif_str, settings) {
	unif_str=gensub(/;/, "\n", "g", settings_string);
	unif_str=gensub(/\n+/, "\n", "g", unif_str);

	split(unif_str, settings, "\n");
	for (i=1; i<=length(settings); i++)
		if (mark_setting(settings[i]) && settings[i] ~ /\S/) {
			found[style_depth]=1;
			add_to_style(create_indentation(settings[i], offset) ";\n");
		}
}

function add_head_to_style (head_string, offset,	unif_str, elements, ending) {
	unif_str=gensub(/\n+/, "\n", "g", head_string);

	split(unif_str, elements, ",\n");
	for (i=1; i<=length(elements); i++) {
		ending=",\n";
		if (i == length(elements)) ending="";
		add_to_style(create_indentation(elements[i], offset) ending);
	}
}

{
	field_index=1;
	for (field_index=1; field_index<NF; field_index++){

		field=$field_index;

		split_index=find_last(field, ";");
		if (split_index) {
			before_with=substr(field, 1, split_index);
			after=substr(field, split_index+1);

			add_settings_to_style(before_with);

			field=after;
		}

		style_depth++;
		style_stack[style_depth]="";
		found[style_depth]=0;
		add_head_to_style(field, -1);
		add_to_style(" {\n");

	}

	add_settings_to_style($field_index);
	style_depth--;

	if (found[style_depth+1]) {
		add_to_style(style_stack[style_depth+1]);
		add_to_style(create_indentation("}") "\n");
	}
}

END {
	if (style_stack[1])
		print style_stack[1];
}
