#!/usr/bin/gawk -f

# ignore empty lines
/^$/ { next; }

# multiline comments
match($0, /\/\*[^\*]*\*\//) {
    $0=substr($0, 1, RSTART-1) substr($0, RSTART+RLENGTH);
}
match($0, /\*\//) && multiline_comment {
    multiline_comment=0;
    $0=substr($0, RSTART+RLENGTH);
}
match($0, /\/\*/) {
    multiline_comment=1;
    $0=substr($0, 1, RSTART-1);
}
{ if(multiline_comment) next; }

# singleline comment
match($0, /\/\//) {
    $0=substr($0, 1, RSTART-1);
}

{ if(length($0)) print; }
