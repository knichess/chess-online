#!/usr/bin/env bash

source_dir="./source"
target_dir="./target"

mkdir -p $source_dir
mkdir -p $target_dir

color_black=${1:-"212121"}
color_white=${2:-"fafafa"}

for source_file in $source_dir/*.svg; do
    file=${source_file:9}

    file_black="$target_dir/${file:0:${#file}-4}-black.${file:${#file}-3}"
    file_white="$target_dir/${file:0:${#file}-4}-white.${file:${#file}-3}"

    sed -E "s/(fill=\"#|fill:#)[0-9a-fA-F]*/\1$color_black/g" $source_file > $file_black
    sed -E "s/(fill=\"#|fill:#)[0-9a-fA-F]*/\1$color_white/g" $source_file > $file_white
done
